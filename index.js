var draw;
var color="#000000";
var size="1";
var pencil=true;
var rec=false;
var cir=false;
var tri=false;
var era=false;
var writting=false;
var img_p=document.getElementById("pen");
var img_e=document.getElementById("eraser");
var img_r=document.getElementById("rec");
var img_c=document.getElementById("round");
var img_t=document.getElementById("tri");
var img_text=document.getElementById("text");
var img_rain=document.getElementById("rainbow");
var x;
var y;
var img_data=new Array;
var index=0;
var Max=0;
var first=true;
var pic=false;
var Rainbow=false;
var r=255,g=0,b=0;
var add=1;

var canvas=document.getElementById("canvas");
var ctx=canvas.getContext("2d");

function mousedown() {
    x=event.offsetX;
    y=event.offsetY;
    
    if(first){
        first=false;
        img_data[0]=ctx.getImageData(0,0,canvas.width,canvas.height);
    } 

    if(writting){
        ctx.font=document.getElementById("text_size").value+"px serif";
        ctx.strokeStyle=color;
        ctx.fillStyle=color;
        ctx.fillText(document.getElementById("input").value,x,y,698-x);
    }
    else{
        ctx.moveTo(event.offsetX, event.offsetY);
        draw=true;
        ctx.beginPath();
    }
}

function mousemove() {
    if(draw) {
        if(pencil){
            if(Rainbow){
                if(r==255 && g==255 && b==255){
                    add=-1;
                }
                else if(r==0 && g==0 && b==0){
                    add=1;
                }
                
                if(r<255 && add==1){
                    r++;
                }
                else if(g<255 && add==1){
                    g++;
                }
                else if(b<255 && add==1){
                    b++;
                }
                
                if(r>0 && add==-1){
                   r--;
                }
                else if(g>0 && add==-1){
                    g--;
                }
                else if(b>0 && add==-1){
                    b--;
                }
                ctx.strokeStyle="rgb("+r+","+g+","+b+")";
            }
            ctx.lineTo(event.offsetX, event.offsetY);
            ctx.stroke();
        }
        
    }
}

function mouseup() {
    if(rec){
        ctx.strokeRect(x,y,event.offsetX-x,event.offsetY-y);
    }
    else if(cir){
        ctx.arc(x,y,Math.sqrt((event.offsetX-x)**2+(event.offsetY-y)**2),0,2*Math.PI);
        ctx.stroke();
    }
    else if(tri){
        ctx.lineTo(x,event.offsetY);
        ctx.lineTo(event.offsetX,event.offsetY);
        ctx.lineTo(x,y);
        ctx.fillStyle=color;
        ctx.fill();
    }
    index++;
    img_data[index]=ctx.getImageData(0,0,canvas.width,canvas.height);
    Max=index;
    draw=false;
    ctx.closePath();
}

function changeColor() {
    color=document.getElementById("color").value;
    ctx.strokeStyle=color;
    if(era){
        ctx.strokeStyle="#ffffff";
    }
}

function changeSize() {
    size=document.getElementById("size").value;
    ctx.lineWidth=size;
}

function pen() {
    canvas.style.cursor="default";
    ctx.strokeStyle=color;
    img_p.src=("Img/pencil_press.png");
    img_e.src=("Img/eraser.png");
    img_r.src=("Img/rec.png");
    img_c.src=("Img/circle.png");
    img_t.src=("Img/triangle.png");
    img_text.src=("Img/text.png");
    img_rain.style.opacity="0.7";
    document.getElementById("input").style.display="none";
    document.getElementById("text_size").style.display="none";
    pencil=true;
    era=false;
    rec=false;
    cir=false;
    tri=false;
    writting=false;
    Rainbow=false;
}

function eraser() {
    canvas.style.cursor="cell";
    ctx.strokeStyle="#ffffff";
    img_e.src=("Img/eraser_press.png");
    img_p.src=("Img/pencil.png");
    img_r.src=("Img/rec.png");
    img_c.src=("Img/circle.png");
    img_t.src=("Img/triangle.png");
    img_text.src=("Img/text.png");
    img_rain.style.opacity="0.7";
    document.getElementById("input").style.display="none";
    document.getElementById("text_size").style.display="none";
    pencil=true;
    era=true;
    rec=false;
    cir=false;
    tri=false;
    writting=false;
    Rainbow=false;
}

function rectangle() {
    canvas.style.cursor="crosshair";
    ctx.strokeStyle=color;
    img_r.src=("Img/rec_press.png");
    img_p.src=("Img/pencil.png");
    img_e.src=("Img/eraser.png");
    img_c.src=("Img/circle.png");
    img_t.src=("Img/triangle.png");
    img_text.src=("Img/text.png");
    img_rain.style.opacity="0.7";
    document.getElementById("input").style.display="none";
    document.getElementById("text_size").style.display="none";
    pencil=false;
    era=false;
    rec=true;
    cir=false;
    tri=false;
    writting=false;
    Rainbow=false;
}

function c() {
    canvas.style.cursor="crosshair";
    ctx.strokeStyle=color;
    img_r.src=("Img/rec.png");
    img_p.src=("Img/pencil.png");
    img_e.src=("Img/eraser.png");
    img_c.src=("Img/circle_press.png");
    img_t.src=("Img/triangle.png");
    img_text.src=("Img/text.png");
    img_rain.style.opacity="0.7";
    document.getElementById("input").style.display="none";
    document.getElementById("text_size").style.display="none";
    pencil=false;
    era=false;
    rec=false;
    cir=true;
    tri=false;
    writting=false;
    Rainbow=false;
}

function triangle() {
    canvas.style.cursor="default";
    ctx.strokeStyle=color;
    img_r.src=("Img/rec.png");
    img_p.src=("Img/pencil.png");
    img_e.src=("Img/eraser.png");
    img_c.src=("Img/circle.png");
    img_t.src=("Img/triangle_press.png");
    img_text.src=("Img/text.png");
    img_rain.style.opacity="0.7";
    document.getElementById("input").style.display="none";
    document.getElementById("text_size").style.display="none";
    pencil=false;
    era=false;
    rec=false;
    cir=false;
    tri=true;
    writting=false;
    Rainbow=false;
}

function w() {
    canvas.style.cursor="text";
    ctx.strokeStyle=color;
    img_r.src=("Img/rec.png");
    img_p.src=("Img/pencil.png");
    img_e.src=("Img/eraser.png");
    img_c.src=("Img/circle.png");
    img_t.src=("Img/triangle.png");
    img_rain.style.opacity="0.7";
    img_text.src=("Img/text_press.png");
    document.getElementById("input").style.display="inline";
    document.getElementById("text_size").style.display="inline";
    pencil=false;
    era=false;
    rec=false;
    cir=false;
    tri=false;
    writting=true;
    Rainbow=false;
}

function rain() {
    canvas.style.cursor="default";
    ctx.strokeStyle=color;
    img_r.src=("Img/rec.png");
    img_p.src=("Img/pencil.png");
    img_e.src=("Img/eraser.png");
    img_c.src=("Img/circle.png");
    img_t.src=("Img/triangle.png");
    img_text.src=("Img/text.png");
    img_rain.style.opacity="1";
    document.getElementById("input").style.display="none";
    document.getElementById("text_size").style.display="none";
    pencil=true;
    era=false;
    rec=false;
    cir=false;
    tri=false;
    writting=false;
    Rainbow=true;
}

function Clear() {
    location.reload();
}

function Download() {
    return document.getElementById('canvas').toDataURL()
}


var Loader = document.getElementById("Load");
    Loader.addEventListener("change", Upload, true);
function Upload(e){  
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            ctx.drawImage(img,0,0,img.width,img.height,0,0,canvas.width,canvas.height);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);      
}


function Undo() {
    index--;
    if(index>=0){
        ctx.putImageData(img_data[index],0,0);
    }
    else{
        index=0;
    }    
}

function Redo() {
    index++;    
    if(Max>=index){
        ctx.putImageData(img_data[index],0,0);
    }
    else {
        index=Max;
        ctx.putImageData(img_data[Max],0,0);
    }    
}

